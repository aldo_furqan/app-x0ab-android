package furqan.albarado.appx0ab

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_prodi.*

class AdapterDataProdi(val dataprod : List<HashMap<String,String>>, val kat : ProdiActivity) : RecyclerView.Adapter<AdapterDataProdi.HolderProdi> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterDataProdi.HolderProdi {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.row_prodi,parent,false)
        return HolderProdi(v)
    }

    override fun getItemCount(): Int {
        return dataprod.size
    }

    override fun onBindViewHolder(holder: AdapterDataProdi.HolderProdi, position: Int) {
        val data =dataprod.get(position)
        holder.txprod.setText(data.get("nama_prodi"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            //val pos = mainActi.daftarprodi.indexOf(data.get("nama_prodi"))
            //mainActivity.SpProdi.setSelection(pos)
            kat.edProdi.setText(data.get("nama_prodi"))
        })
    }




    class HolderProdi(v : View) :  RecyclerView.ViewHolder(v){

        val txprod = v.findViewById<TextView>(R.id.txtProdi)
        val layouts = v.findViewById<ConstraintLayout>(R.id.cProdi)
    }

}