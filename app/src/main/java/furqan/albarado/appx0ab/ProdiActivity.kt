package furqan.albarado.appx0ab

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_prodi.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class ProdiActivity : AppCompatActivity() , View.OnClickListener {

    lateinit var adapProdi: AdapterDataProdi
    var daftarprodi = mutableListOf<HashMap<String,String>>()
    var uri1 = "http://192.168.43.170/www/kampus/show_prodi.php"
    var uri2 = "http://192.168.43.170/www/kampus/query_prodi.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_prodi)

        adapProdi =  AdapterDataProdi(daftarprodi,this)
        lsProdi.layoutManager = LinearLayoutManager(this)
        lsProdi.adapter = adapProdi

        btnInsert.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        Showprod("")
    }


    fun Showprod(namaProd: String){
        val request = object : StringRequest(
            Request.Method.POST, uri1, Response.Listener{ response ->
                daftarprodi.clear()
                val jsonArray = JSONArray(response)
                for (x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var prodi = HashMap<String,String>()
                    prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
//                prodi.put("id_prodi",jsonObject.getInt("id_prodi").toString())
                    daftarprodi.add(prodi)
                }
                adapProdi.notifyDataSetChanged()
            },
            Response.ErrorListener{ error ->
                Toast.makeText(this, "Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String, String>()
                hm.put("nama_prodi", namaProd)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


//    fun Showprod(){
//        val request = StringRequest(Request.Method.POST,uri1, Response.Listener { response ->
//            daftarprodi.clear()
//            val jsonArray = JSONArray(response)
//            for (x in 0..(jsonArray.length()-1)){
//                val jsonObject = jsonArray.getJSONObject(x)
//                var prodi = HashMap<String,String>()
//                prodi.put("nama_prodi",jsonObject.getString("nama_prodi"))
//                prodi.put("id_prodi",jsonObject.getInt("id_prodi").toString())
//                daftarprodi.add(prodi)
//            }
//            adapProdi.notifyDataSetChanged()
//        }, Response.ErrorListener { error ->
//            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
//        })
//        val queue =  Volley.newRequestQueue(this)
//        queue.add(request)
//    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri2,
            Response.Listener { response ->
                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    Showprod("")
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                // var nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault()).format(
                //     Date()
                // )+".jpg"
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_prodi",edProdi.text.toString())

                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_prodi",edProdi.text.toString())
                    }
                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("nama_prodi",edProdi.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
        Showprod("")
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert->{
                query("insert")
            }
            R.id.btnDelete->{
                query("delete")
            }
            R.id.btnUpdate-> {
                query("update")
            }
        }
    }
}



