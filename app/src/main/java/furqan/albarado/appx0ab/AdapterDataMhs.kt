package furqan.albarado.appx0ab

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_mhs.*

class AdapterDataMhs(val dataMhs : List<HashMap<String, String>>,
                     val mhsActivity: MhsActivity) :
    RecyclerView.Adapter<AdapterDataMhs.HolderDataMhs>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterDataMhs.HolderDataMhs {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_mhs,p0,false)
        return HolderDataMhs(v)
    }

    override fun getItemCount(): Int {
        return dataMhs.size
    }

    override fun onBindViewHolder(p0: AdapterDataMhs.HolderDataMhs, p1: Int) {
        val data = dataMhs.get(p1)
        p0.txNim.setText(data.get("nim"))
        p0.txNama.setText(data.get("nama"))
        p0.txProdi.setText(data.get("nama_prodi"))
        p0.txAlamat.setText(data.get("alamat"))
        //begin new
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener(View.OnClickListener {
            val pos = mhsActivity.daftarProdi.indexOf(data.get("nama_prodi"))
            mhsActivity.spinProdi.setSelection(pos)
            mhsActivity.edNim.setText(data.get("nim"))
            mhsActivity.edNamaMhs.setText(data.get("nama"))
            mhsActivity.edAlamat.setText(data.get("alamat"))
            Picasso.get().load(data.get("url")).into(mhsActivity.imgUpload);
        })
        //end new
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }

    class HolderDataMhs(v : View) : RecyclerView.ViewHolder(v) {
        val txNim = v.findViewById<TextView>(R.id.txNim)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txProdi = v.findViewById<TextView>(R.id.txProdi)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout)
        val txAlamat = v.findViewById<TextView>(R.id.txAlamat)
    }
}