package furqan.albarado.appx0ab

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity  : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMhs->{
                val show =  Intent(this,MhsActivity::class.java)
                startActivity(show)
            }
            R.id.btnProdi->{
                val show = Intent(this, ProdiActivity::class.java)
                startActivity(show)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnMhs.setOnClickListener(this)
        btnProdi.setOnClickListener(this)

    }
}